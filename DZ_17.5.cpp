
#include <vector>
#include <iostream>

class Example
{
private:
    int a;
    
public:
    int GetA() 
    {
        return a;
    };
    void SetA(int newA) 
    {
        a = newA;

    };
};
class vector
{
public:
    vector() :x(0), y(0), z(0)
    {}
    vector(double _x, double _y, double _z): x(_x), y(_y),z(_z)
    {}
    void Show()
    {
        std::cout << x << ' ' << y << ' ' << z << '\n';
    }

private:
    double x;
    double y;
    double z;
};
int main()
{
    Example temp, temp1;

    temp.SetA(1);
    temp1.SetA(8);
    std::cout << temp.GetA() << ' ' << temp1.GetA() << '\n';

    int a;
    int b;
    int c;

    a = 1;
    b = 2;
    c = 8;
    vector v(a, b, c);
    v.Show();


    float len = sqrt(a*a+b*b+c*c);
    std::cout << "|v| ="  << len << '\n';

    

   
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
